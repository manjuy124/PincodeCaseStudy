import React, { Component } from 'react';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import logo from './logo.svg';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';

import MainScreen from './MainScreen';
import App from './App';

import './App.css';

class HomeScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      pincode: '',
      baseUrl: 'http://postalpincode.in/api/pincode/',
      district: '',
      region: '',
      state: '',
      country: '',
      postOfficeMessage: ''
    }
  }

  handleClick(event) {

    if (this.state.pincode.length == 6) {
      var url = this.state.baseUrl + this.state.pincode;

      fetch(url, {
        method: 'GET',
      }).then(response => { return response.json(); })
        .then(responseData => { return responseData; })
        .then(data => {
          let postOfficeMessage = data["Message"];
          let firstObject = data["PostOffice"][0];
          let district = firstObject["District"];
          let region = firstObject["Region"];
          let state = firstObject["State"];
          let country = firstObject["Country"];
          this.setState({
            district: district, region: region, state: state,
            country: country, postOfficeMessage: postOfficeMessage
          });
        })
        .catch(function (err) {
          if (err == "TypeError: Failed to fetch") {
            alert('Please add and enable Allow-Control-Allow-Origin extension to use this application! Once you added the extension please restart the application.');
          } else {
            alert('Please enter valid Pin Code.');
          }
        });
    }
    else {
      alert('Please enter valid Pin Code.');
    }
  }

  onSignOut(event) {
    var loginPage = [];
    loginPage.push(<MainScreen appContext={this.props.appContext} />);
    this.props.appContext.setState({ loginPage: loginPage, registerScreen: [] })
  }

  render() {
    return (

      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to PinCode React Application</h1>
        </header>
        <p className="App-intro">
          Please enter the PINCODE to get the information.
        </p>
        <div>
          <MuiThemeProvider>
            <div>
              <TextField
                hintText="Enter PINCODE"
                floatingLabelText="Pincode"
                onChange={(event, newValue) => this.setState({ pincode: newValue })}
              />
              <br />
              <RaisedButton label="Submit" primary={true} style={style} onClick={(event) => this.handleClick(event)} />
            </div>
            <br />
            <br />
            <label >*** Pincode Information ***</label> <br /> <br />

            <div style={{ width: '100%' }}>
              <MuiThemeProvider>
                <Table style={{ width: 600, margin: 'auto' }}>
                  <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
                    <TableRow>
                      <TableHeaderColumn>District</TableHeaderColumn>
                      <TableHeaderColumn>Region</TableHeaderColumn>
                      <TableHeaderColumn>State</TableHeaderColumn>
                      <TableHeaderColumn>Country</TableHeaderColumn>
                    </TableRow>
                  </TableHeader>
                  <TableBody displayRowCheckbox={false}>
                    <TableRow>
                      <TableRowColumn>{this.state.district}</TableRowColumn>
                      <TableRowColumn>{this.state.region}</TableRowColumn>
                      <TableRowColumn>{this.state.state}</TableRowColumn>
                      <TableRowColumn>{this.state.country}</TableRowColumn>
                    </TableRow>
                    <TableRow>
                      <TableRowColumn colSpan={2}>{this.state.postOfficeMessage}</TableRowColumn>
                    </TableRow>
                  </TableBody>
                </Table>
              </MuiThemeProvider>
            </div>
            <button style={TextStyle} onClick={(event) => this.onSignOut(event)}>Sign Out</button>
          </MuiThemeProvider>
        </div>
      </div>
    );
  }
}

const style = {
  margin: 15,
};

const TextStyle = {
  textAlign: 'center',
  fontSize: 16,
  textDecorationLine: 'underline',
  borderBottom: 'none',
  borderTop: 'none',
  borderLeft: 'none',
  borderRight: 'none',
  margin: 10,
  outline: 'none'
};

const customColumnStyle = { width: 50, backgroundColor: 'yellow' };


export default HomeScreen;