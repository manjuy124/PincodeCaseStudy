import React, { Component } from 'react';

import MainScreen from './MainScreen';
import injectTapEventPlugin from 'react-tap-event-plugin';
import './App.css';

// injectTapEventPlugin - For FastClick Feature
injectTapEventPlugin();

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      loginPage: [],
      registerScreen: []
    }
  }

  componentWillMount() {
    var loginPage = [];
    loginPage.push(<MainScreen appContext={this} />);
    this.setState({
      loginPage: loginPage
    })
  }

  render() {
    return (
      <div className="App">
        {this.state.loginPage}
        {this.state.registerScreen}
      </div>
    );
  }

}

const style = {
  margin: 15,
};
export default App;
