import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import RaisedButton from 'material-ui/RaisedButton';
import Login from './Login';
import Register from './Register';

class MainScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: '',
      password: '',
      loginscreen: [],
      loginmessage: '',
      buttonLabel: 'Register Here!',
      isLogin: true
    }
  }

  componentWillMount() {
    var loginscreen = [];
    loginscreen.push(<Login parentContext={this} appContext={this.props.appContext} />);
    var loginmessage = "Not Registered yet?";
    this.setState({
      loginscreen: loginscreen,
      loginmessage: loginmessage
    })
  }

  handleClick(event) {
    var loginmessage;
    if (this.state.isLogin) {
      var loginscreen = [];
      loginscreen.push(<Register parentContext={this} appContext={this.props.appContext} />);
      loginmessage = "Already registered?";
      this.setState({
        loginscreen: loginscreen,
        loginmessage: loginmessage,
        buttonLabel: "Login Here!",
        isLogin: false
      })
    }
    else {
      var loginscreen = [];
      loginscreen.push(<Login parentContext={this} appContext={this.props.appContext} />);
      loginmessage = "Not Registered yet?";
      this.setState({
        loginscreen: loginscreen,
        loginmessage: loginmessage,
        buttonLabel: "Register Here!",
        isLogin: true
      })
    }
  }

  onSignOut() {
  }

  render() {
    return (
      <div className="loginscreen">
        {this.state.loginscreen}
        <div>
          <MuiThemeProvider>
            {this.state.loginmessage}
            <button label={this.state.buttonLabel} primary={true} style={TextStyle} onClick={(event) => this.handleClick(event)}>{this.state.buttonLabel}</button>
          </MuiThemeProvider>
        </div>
      </div>
    );
  }
}
const style = {
  margin: 15
};

const TextStyle = {
  textAlign: 'center',
  fontSize: 16,
  textDecorationLine: 'underline',
  borderBottom: 'none',
  borderTop: 'none',
  borderLeft: 'none',
  borderRight: 'none',
  margin: 10,
  outline: 'none'
};
export default MainScreen;