import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import Login from './Login';

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      firstname: '',
      lastname: '',
      email: '',
      password: ''
    }
  }

  handleClick(event) {

    var url = 'http://localhost:3000/register';
    if (this.state.username != 0 || this.state.firstname != 0 || this.state.lastname != 0 || this.state.email != 0 || this.state.password) {
      if (this.validateEmail(this.state.email)) {
        var payload = {
          'Username': this.state.username,
          'Firstname': this.state.firstname,
          'Lastname': this.state.lastname,
          'Email': this.state.email,
          'Password': this.state.password
        };
        var jsonString = JSON.stringify(payload);
        fetch(url, {
          method: 'post',
          headers: {
            'Content-Type': 'application/json'
          },
          body: jsonString
        }).then(response => { 
          if(response.status == 404) {
            alert('Please run the back end Application as well!');
          } else {
            return response.json();
          }
        }).then(responseData => { return responseData; })
          .then(data => {
            if (data["code"] == 201) {
              var loginscreen = [];
              loginscreen.push(<Login parentContext={this} appContext={this.props.parentContext} />);
              var loginmessage = "Not Registered yet?";
              this.props.parentContext.setState({
                loginscreen: loginscreen,
                loginmessage: loginmessage,
                buttonLabel: "Register Here!",
                isLogin: true
              });
            }
            alert(data["message"]);
          })
          .catch(function (err) {
            console.log(err);
          });
      } else {
        alert('Please enter valied email.');
      }
    } else {
      alert('Please enter all the fields.');
    }

  }

  // Function to Validate the Email
  // Returns TRUE if entered Email is Valid ; Otherwise FALSE
  validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  render() {
    return (
      <div>
        <MuiThemeProvider>
          <div>
            <AppBar
              title="Register"
              showMenuIconButton={false}
            />
            <TextField
              hintText="Enter Username"
              floatingLabelText="User Name"
              onChange={(event, newValue) => this.setState({ username: newValue })}
            />
            <br />
            <TextField
              hintText="Enter your First Name"
              floatingLabelText="First Name"
              onChange={(event, newValue) => this.setState({ firstname: newValue })}
            />
            <br />
            <TextField
              hintText="Enter your Last Name"
              floatingLabelText="Last Name"
              onChange={(event, newValue) => this.setState({ lastname: newValue })}
            />
            <br />
            <TextField
              hintText="Enter your Email"
              type="email"
              floatingLabelText="Email"
              onChange={(event, newValue) => this.setState({ email: newValue })}
            />
            <br />
            <TextField
              type="password"
              hintText="Enter your Password"
              floatingLabelText="Password"
              onChange={(event, newValue) => this.setState({ password: newValue })}
            />
            <br />
            <RaisedButton label="Submit" primary={true} style={style} onClick={(event) => this.handleClick(event)} />
          </div>
        </MuiThemeProvider>
      </div>
    );
  }
}
const style = {
  margin: 15,
};
export default Register;