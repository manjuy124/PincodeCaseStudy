import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';

import HomeScreen from './HomeScreen';

class Login extends Component {
constructor(props){
  super(props);
  this.state={
    username:'',
    password:''
  }
 }


handleClick(event){
    var url = 'http://localhost:3000/login';
    if(this.state.username != 0 || this.state.password != 0) {

      var payload = {
        'Username': this.state.username,
        'Password': this.state.password
      };

      var jsonString = JSON.stringify( payload );
      fetch(url, {
      method: 'post',
      headers: {
        'Content-Type': 'application/json'
      },
      body: jsonString
      }).then(response => { 
        if(response.status == 404) {
          alert('Please run the back end Application as well!');
        } else {
          return response.json();
        }
      })
      .then(responseData => { return responseData;})
      .then(data => { 
        if(data["code"] == 200) {
          var homeScreen = [];
          homeScreen.push(<HomeScreen parentContext={this} appContext={this.props.appContext}/>);
          this.props.appContext.setState({loginPage:[],registerScreen:homeScreen})
        } else{
           alert(data["message"]);
        }
      })
      .catch(function(err) {
          console.log(err);
      });
    } else {
      alert('Please enter all the fields.');
    }
}

render() {
    return (
      <div>
        <MuiThemeProvider>
          <div>
          <AppBar
             title="Login"
             showMenuIconButton={false}
           />
           <TextField
             hintText="Enter your Username"
             floatingLabelText="Username"
             onChange = {(event,newValue) => this.setState({username:newValue})}
             />
           <br/>
             <TextField
               type="password"
               hintText="Enter your Password"
               floatingLabelText="Password"
               onChange = {(event,newValue) => this.setState({password:newValue})}
               />
             <br/>
             <RaisedButton label="Submit" primary={true} style={style} onClick={(event) => this.handleClick(event)}/>
         </div>
         </MuiThemeProvider>
      </div>
    );
  }
}
const style = {
 margin: 15,
};
export default Login;