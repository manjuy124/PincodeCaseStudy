var express    = require("express");
var login = require('./routes/loginroutes');
var bodyParser = require('body-parser');
 
var app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
 
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
var router = express.Router();
 
// test route
router.get('/', function(req, res) {
    res.json({ message: 'welcome to our upload module apis' });
});
 
//route to handle user registration
router.get('/forgot', function(req, res) {
    res.render('forgot', {
      user: req.user
    });
  });
const PORT = process.env.PORT || 3000;

// Register Route  
app.post('/register',login.register);

// Login Route
app.post('/login',login.login);

app.listen(PORT, function () {
    console.log('Example app listening on port 3000!');
  });
