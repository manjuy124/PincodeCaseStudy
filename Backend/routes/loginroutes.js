var sqlite3 = require('sqlite3').verbose();

var db = new sqlite3.Database('Auth.db');

// *************** Response Codes **********************
// CODE : 200 - Login Successful!
// CODE : 202 - Problem with Database!
// CODE : 205 - Username and password does not match!
// CODE : 206 - Username and password does not exist!

// CODE : 201 - SiginUp Successful!
// CODE : 202 - Problem with Database!
// CODE : 203 - Email ID Already Exists!
// CODE : 204 - Username Already Exists!

// ****************************************************

var today = new Date();

exports.register = function (req, res) {

  var today = new Date();
  var username = req.body.Username;
  var email = req.body.Email;
  var users = {
    "Username": req.body.Username,
    "Firstname": req.body.Firstname,
    "LastName": req.body.Lastname,
    "Email": req.body.Email,
    "Password": req.body.Password,
    "Timestamp": today
  }
  let usernameQuery = `SELECT * FROM User WHERE Username  = ?`;
  let emailIdQuery = `SELECT * FROM User WHERE Email  = ?`;

  db.all(usernameQuery, username, function (err, row) {
    if (row.length > 0) {
      res.send({
        "code": 204,
        "message": "Username Already Exists!"
      })
    } else {
      db.all(emailIdQuery, email, function (err, row) {
        if (row.length > 0) {
          res.send({
            "code": 203,
            "message": "Email ID Already Exists!"
          })
        } else {
          db.run("INSERT INTO User (username, Firstname, Lastname, Email, Password, Timestamp) VALUES (?, ?, ? , ?, ? , ? )", [users.Username, users.Firstname, users.Lastname, users.Email, users.Password, users.Timestamp], function (error, results, fields) {
            if (error) {
              res.send({
                "code": 202,
                "message": "Problem with Database!"
              })
            } else {
              res.send({
                "code": 201,
                "message": "SiginUp Successful!"
              });
            }
          });
        }
      });
    }
  });

}
exports.login = function (req, res) {
  var user = req.body.Username;
  var password = req.body.Password;

  let sql = `SELECT * FROM User WHERE Username  = ?`;
  
  db.all(sql, user, function (err, row) {
    if (err) {
      res.send({
        "code": 202,
        "message": "Problem with Database!"
      })
    } else {
      if (row.length > 0) {
        if (row[0].Password == password) {
          res.send({
            "code": 200,
            "message": "login sucessfull"
          });
        }
        else {
          res.send({
            "code": 205,
            "message": "Username and password does not match"
          });
        }
      } else {

        res.send({
          "code": 206,
          "message": "Username and password does not exist"
        });
      }
    }
  });
}
