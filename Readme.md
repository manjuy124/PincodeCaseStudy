## For running this project, we need Node.js and chrome installed on your computer.

## Following frameworks/libraries are used in the applicatoin.

For Backend :      

i.  	Sqlite (Database)		- 	DB
                    
ii. 	node.js					-	Framework
                    
iii.	http://postalpincode.in/api/pincode/{YOUR_PINCODE}		-	API


For Frontend : 		

i.		React.js 				-	Framework

ii. 	Material UI				-	Package

iii.	Create React App		-	Package


## Steps in Executing the Project:

1. Add the following extension to Chrome and enable the extension.
	    - 	Allow-Control-Allow-Origin
	    - 	
2. Open the Backend Folder with terminal/cmd and run the following commands

	    i.	    npm install
	    ii. 	npm run start
	    
3. Open the Frontend Folder with terminal/cmd and run the following commands

	    i.	    npm install
	    ii. 	npm run start


## More Information:

1. Once you run the frontend application, you can see new opened tab in chrome.

2. You can use following credentials to move to the Home Screen.
    Username : Manjunath
    Password : ttt

3. Once you entered Home Screen, you can see textfield with placeholder 'Enter Pincode.'.

4. Make sure that you added and enabled the extension which is specified above.

5. Once you enter valid pincode, you can see basic information like District, Region.. on the screen.
